Core library for MagicTune - Automatic optimization and calibration technology applied to various applications and services.

The purpose of the library is to serve as the look behind and look ahead engine to automatically tune a configuration file or directives.

Imagine installing MySQL and postinstall checks the system, environment and other capabilities to give you a matching set of defaults for the 
MySQL configuration file. Then it tests the config and autotunes it based on the load of MySQL over time.

Each parameter app setting affects a related app setting or a corresponding OS setting which is the look ahead part. Similarly, system
configuration upon install is the look behind part.

Let's go for optimum!